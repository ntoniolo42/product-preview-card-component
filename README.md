# Frontend Mentor - Product preview card component solution

This is a solution to the [Product preview card component challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/product-preview-card-component-GO7UmttRfa). Frontend Mentor challenges help you improve your coding skills by building realistic projects. 

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Screenshot](#screenshot)
  - [Links](#links)
  - [What I learned](#what-i-learned)
- [Author](#author)

## Overview

### The challenge

Users should be able to:

- View the optimal layout depending on their device's screen size
- See hover and focus states for interactive elements

### Screenshot

![](./screenshots/screenshot.png)

### Links

- [Solution](https://gitlab.com/ntoniolo42/product-preview-card-component)
- [Live Site](https://frontendmentor.product-preview-card-component.ntoniolo.wtf/)


### What I learned

- [reset css](https://github.com/elad2412/the-new-css-reset)
- [BEM](https://css-tricks.com/bem-101/)
- [picture](https://developer.mozilla.org/fr/docs/Web/HTML/Element/picture) with multiples sources

## Author

- Website - [ntoniolo.wtf](https://ntoniolo.wtf)
- Frontend Mentor - [@On-Jin](https://www.frontendmentor.io/profile/On-Jin)
